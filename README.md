# Time Spent in University (TSiU)

[![N|Solid](http://i.imgur.com/wG4WIk7.png")](https://tsiu.ga/)

TSiU is a social network like indoor monitor system. Its main purpose is to analyze the building rooms usage in a game like fashion. The analysis could be used to better optimize the rooms usage planning.

Working demo [here](https://tsiu.ga/).

## Intallation

The server needs an HTTP server and a DBMS installed on it.
We have used MySQL and Apache2 (its working dir is */var/www/html/*).

```sh
$ git clone <repoURL>
# cd /var/www/html/ && \
> cp -R <repo_PATH>/server/* . && \
> cp -R <repo_PATH>/client/* public/pwa/
```

The *db.sql* file can be found at *<repoPATH>/server/*.
```sh
$ mysql -u <username> -p
$ mysql <db_name> < db.sql
```

## CloudFlare configuration

Configure CloudFlare as a common website.
Then add two simple rules:

| URL Match | Rule |
| ------ | ------ |
| http://*example\.com/\* | Always Use HTTPS |
| \*example\.com/pwa/service\-worker\.js | Cache Level: Bypass |

The first rule is used to ensure content on example.com is always loaded over HTTPS (SW requirement). The second one is needed to make sure Cloudflare does not cache serviceworker.js, as it would by default for the CDN, we instructed Cloudflare to bypass the cache.

### Tech
Tsiu uses a the following open source projects to work properly:

* [Lumen](https://lumen.laravel.com/) - a PHP framework
* [Snap.svg](http://snapsvg.io/) - a JavaScript SVG library
* [jQuery](https://jquery.com/) - a JavaScript library
* [ToastJS](http://toast-js.com/) - a Javascript Toast Notification Library