var dataCacheName = 'weatherData-v1';

var cacheName = 'weatherPWA-step-6-1';
var filesToCache = [
  '/',
  '/index.html',
  '/css/animations.css',
  '/css/style.css',
  '/css/toast-js.min.css',
  '/images/chrome-touch-icon-192x192.png',
  '/images/chrome-touch-icon-512x512.png',
  '/js/modernizr.custom.js',
  '/js/pagetransitions.js',
  '/js/toast.min.js',
  '/favicon.ico'
];

var scritpsToCache = [
  '/scripts/jquery.min.js',
  '/scripts/snap.svg-min.js',
  '/scripts/itemList.js',
  '/scripts/store.js',
  '/scripts/animationFunction.js',
  '/scripts/core/app.js'
];

var svgsToCache = [
    '/res/baffi1.svg',
    '/res/baffi2.svg',
    '/res/baffi3.svg',
    '/res/baffi4.svg',
    '/res/body.svg',
    '/res/book.svg',
    '/res/capelli1.svg',
    '/res/capelli1W.svg',
    '/res/capelli2.svg',
    '/res/capelli2W.svg',
    '/res/capelli3.svg',
    '/res/capelli3W.svg',
    '/res/capelli4.svg',
    '/res/capelli4W.svg',
    '/res/capelli5.svg',
    '/res/capelli5W.svg',
    '/res/casual-t-shirt-.svg',
    '/res/delete.svg',
    '/res/face.svg',
    '/res/female.svg',
    '/res/fiocco1.svg',
    '/res/fiocco2.svg',
    '/res/go-back.svg',
    '/res/male.svg',
    '/res/naso1.svg',
    '/res/naso1W.svg',
    '/res/naso2.svg',
    '/res/naso3.svg',
    '/res/occhiali1.svg',
    '/res/occhiali2.svg',
    '/res/occhiali3.svg',
    '/res/occhi.svg',
    '/res/orecchini1.svg',
    '/res/orecchini2.svg',
    '/res/orecchini3.svg',
    '/res/pc.svg',
    '/res/pen.svg',
    '/res/templates/menuFemmina.tmplt',
    '/res/templates/menuMaschio.tmplt',
    '/res/templates/femminaSVG.tmplt',
    '/res/templates/maschioSVG.tmplt'
];

// merge arrays
[scritpsToCache, svgsToCache].forEach(function(array){
  filesToCache.push.apply(filesToCache, array);
});
// console.log(filesToCache);


// force HTTPS
if(self.location.hostname !== "localhost"){
  filesToCache.forEach(function(filePath, index, filesToCache) {
    filesToCache[index] = "https://tsiu.ga/pwa" + filePath;
  });
}



/**
*   Install ServiceWorker
*/
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});


/**
*   Activate ServiceWorker
*/
self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate', self);
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});


/**
*   Fetch network requests
*/
self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch', e.request.url);
  var dataUrl = 'tsiu.ga/api';
  console.log('[Service Worker] dataUrl', e.request.url.indexOf(dataUrl));
  if (e.request.url.indexOf(dataUrl) > -1) {
    /*
     * When the request URL contains dataUrl, the app is asking for fresh
     * weather data. In this case, the service worker always goes to the
     * network and then caches the response. This is called the "Cache then
     * network" strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
     */
    e.respondWith(
      caches.open(dataCacheName).then(function(cache) {
        return fetch(e.request).then(function(response){
          cache.put(e.request.url, response.clone());
          return response;
        });
      })
    );
  } else {
    /*
     * The app is asking for app shell files. In this scenario the app uses the
     * "Cache, falling back to the network" offline strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
     */
    e.respondWith(
      caches.match(e.request).then(function(response) {
        return response || fetch(e.request);
      })
    );
  }
});



/**
*   On Notification Click
*/
self.addEventListener('notificationclick', function(e) {
  var notification = e.notification;
  var primaryKey = notification.data.primaryKey;
  var action = e.action;

  if (action === 'close') {
    notification.close();
  } else {
    // clients.openWindow("/");
    // notification.close();
    e.waitUntil(clients.matchAll({
      type: "window"
    }).then(function(clientList) {

      // clientList.forEach(function(client){
      //   console.log(client.url, 'focus' in client);
      //   if (client.url == '/' && 'focus' in client)
      //       console.log("client.focus()");
      //       //return client.focus();
      // });
      console.log(clients);
      console.log("#CL: ", clientList.length, clientList);
      for (var i = 0; i < clientList.length; i++) {
        var client = clientList[i];
        if (/*client.url == '/' &&*/ 'focus' in client)
          console.log(client, " client.focus()");
          return client.focus();
      }

      if (clients.openWindow){
          console.log(clients, " clients.openWindow('/')");
          return clients.openWindow('/');
      }
    }));
  }
});
