(function() {
  'use strict';

  // for API
  const SERVER = "https://tsiu.ga";

  function nFormatter(num, digits) {
     var si = [
       { value: 1E18, symbol: "E" },
       { value: 1E15, symbol: "P" },
       { value: 1E12, symbol: "T" },
       { value: 1E9,  symbol: "G" },
       { value: 1E6,  symbol: "M" },
       { value: 1E3,  symbol: "k" }
     ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
     for (i = 0; i < si.length; i++) {
       if (num >= si[i].value) {
         return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
       }
     }
     return num.toFixed(digits).replace(rx, "$1");
  }

  var timer =  {
      obj: null,
      task: null,
      start: null,
      stop: null,
      startedAt: null,
      interval: null
  }

  function clearTimers(arrayTimers){
    arrayTimers.forEach(function(timer){
      clearInterval(timer);
    });
  }

  function cloneObj(obj){
    return JSON.parse(JSON.stringify(obj));
  }


  /*#########################################################################################
  */



  var app = {
    isLoading: true,
    userData: {
      getProfile: null,
      storeProfile: null
    },
    timers: {
      scoreIncTimer: cloneObj(timer),
      scoreSaverTimer: cloneObj(timer),
      animateEyesTimer: cloneObj(timer),
      animateHeadTimer: cloneObj(timer),
      startTimers: null,
      clearTimers: function(arrayTimers){
        arrayTimers.forEach(function(timer){
          clearInterval(timer);
        });
      },
      started: false
    },
    visibleRooms: {},
    selectedCities: [],
    score: document.getElementById('score'),
    spinner: document.querySelector('.loader'),
    pages: {
      login: document.getElementById('preIndex'),
      homepage: document.getElementById('homepage'),
      hide: function(page) {
        page.classList.remove('pt-page-current');
      },
      show: function(page) {
        page.classList.add('pt-page-current');
      }
    },
    svg: {
      domElem: function (id){
        return document.getElementById(id);
      },
      store: function (id){
        localStorage.setItem("svg", this.domElem(id).innerHTML);
      },
      loadintoElem: function (svgElem){
        svgElem.innerHTML =  localStorage.getItem("svg");
      }
    },
    menu: {
      domElem: document.getElementById("storeMenu"),
      restore: function(){
        var menu = document.getElementById("storeMenu");
        menu.outerHTML = localStorage.getItem("menu");
        interactiveMenu();
        console.log("menu RESTORED");
      },
      store: function(){
        localStorage.setItem("menu", document.getElementById("storeMenu").outerHTML);
        console.log("menu STORED");
      }
    },
    checkInOut: function(InOut){
      var butAdd = document.getElementById('butAdd');
      var selectBeaconToAdd = document.getElementById('selectBeaconToAdd');
      if(InOut == 0){
        butAdd.classList.remove("green");
        butAdd.classList.add("red");
      }else{
        butAdd.classList.remove("red");
        butAdd.classList.add("green");
      }
      selectBeaconToAdd.setAttribute('data-checkInOut', InOut);
      butAdd.innerHTML = InOut == 1 ? "Check-In" : "Check-Out";
    },
    scoreBox: document.getElementById('ScoreBox'),
    roomItemTemplate: document.querySelector('.roomItemTemplate'),
    roomContainer: document.querySelector('#roomsList'),
    addDialog: document.querySelector('.dialog-container'),
    dialogPurchase: document.getElementsByClassName('.dialog-container-purchase')[0]
  };

  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

   document.getElementById('buyItem').addEventListener('click', function() {
     if(app.userData.getFromLS("checkIn")){
       showToast("You can not purchase during Check-In.")
       return;
     }

     var scoreElem = document.getElementById("score");
     var price = this.getAttribute('data-current-price');
     var caller = document.getElementById(this.getAttribute("data-caller-id"));
     var storedProfile = app.userData.getFromLS("profile");
     if(price <= storedProfile.score){
       storedProfile.score -= price;
       profile = cloneObj(storedProfile);
       app.userData.storeProfile(profile);
       scoreElem.setAttribute('data-score', profile.score);
       scoreElem.textContent = nFormatter(profile.score, 2);
       caller.classList.remove('unpurchased');
       app.menu.store();
     }else{
       showToast("Your balance is not enough to purchase this item.")
     }
     console.log(this.getAttribute('data-current-price'));
   });

   document.getElementById('butCreateUser').addEventListener('click', function() {
     console.log("::butCreateUser pressed");
     app.createUser();
   });

   document.getElementById('saveCharSVG').addEventListener('click', function() {
     var charStore = document.getElementById("charStore");
     app.svg.domElem("char").innerHTML = charStore.innerHTML;
     moveEyes("#char", 0, 8, 600);
     app.svg.store("char");
     console.log(":: saveCharSVG ");
   });

   document.getElementById('butRooms').addEventListener('click', function() {
     console.log("::butRooms pressed");
     app.updateAllRooms();
   });

   document.getElementById('butAdd').addEventListener('click', function() {
     // Open/show the add new city dialog
     if(app.userData.getFromLS("checkIn")){
       console.log(":: checkOut");
       app.checkOut();
     }else{
       app.toggleAddDialog(true);
     }
   });

   document.getElementById('selectBeacon').addEventListener('click', function() {
     // Add the newly selected city
     var select = document.getElementById('selectBeaconToAdd');
     var selected = select.options[select.selectedIndex];
     var key = selected.value;
     var label = selected.textContent;
    //  console.log(key, label);
     if(select.getAttribute("data-checkInOut") == 1){
        app.checkIn(key);
     }else{
        console.log(":: selectBeacon  === checkOut");
        //app.checkOut();
     }

     app.toggleAddDialog(false);
   });

   document.getElementById('selectCancel').addEventListener('click', function() {
     // Close the add new city dialog
     app.toggleAddDialog(false);
   });


  /*****************************************************************************
   *
   * Methods to update/refresh the UI
   *
   ****************************************************************************/

  // Toggles the visibility of the add new city dialog.
  app.toggleAddDialog = function(visible) {
    if (visible) {
      app.addDialog.classList.add('dialog-container--visible');
    } else {
      app.addDialog.classList.remove('dialog-container--visible');
    }
  };


  app.timers.animateEyesTimer.task = function(){
    blinkEyes("#char", profile.sex);
  };

  app.timers.animateHeadTimer.task = function(){
    if(document.getElementById("writing").getAttribute("style").indexOf("1") > -1){
      movePen("#char", profile.sex);
    }
    moveHead("#char", profile.sex);
  };

  app.timers.scoreSaverTimer.task = function() {
    var scoreEarned = profile.score;
    var storedProfile = app.userData.getFromLS("profile");
    storedProfile.score = scoreEarned;
    app.userData.storeProfile(storedProfile);
    console.log(":: Score Saved ::");
  };

  app.timers.scoreIncTimer.task = function() {
    var elapsedTime = Date.now() - this.startedAt;
    var currentScore = profile.score + Math.floor(elapsedTime / this.interval);
    // console.log(profile.score + Math.floor(elapsedTime / this.interval));

    var formattedScore = nFormatter(Number(currentScore), 2);
    var scoreString = `${formattedScore}`;

    app.score.setAttribute('data-score', currentScore);
    app.score.textContent = scoreString;
    // profile.score = currentScore;
    // console.log("elapsed: ", elapsedTime, " score: ", currentScore);
  };

  app.timers.startTimers = function(isCheckIn=false, isAnimationOnly=false){
    var timers = JSON.parse(localStorage.getItem("timers"));
    if(timers){
      /* resume timers */
      // svg
      if(isCheckIn === false){
        this.animateEyesTimer.interval =  timers.animateEyesTimer.interval;
        this.animateHeadTimer.interval =  timers.animateHeadTimer.interval;
        this.scoreIncTimer.startedAt =  timers.scoreIncTimer.startedAt;
      }else{
        this.scoreIncTimer.startedAt = Date.now();
        localStorage.setItem("timers", JSON.stringify(this));
      }
      this.scoreIncTimer.interval =  timers.scoreIncTimer.interval;
      // score
      if(isAnimationOnly === false){
        // this.scoreIncTimer.startedAt = Date.now();
        this.scoreIncTimer.interval =  timers.scoreIncTimer.interval;
      }

      // localStorage.setItem("timers", JSON.stringify(this));
      // this.scoreSaverTimer.startedAt =  timers.scoreSaverTimer.startedAt;
      // this.scoreSaverTimer.interval =  timers.scoreSaverTimer.interval;
    }else{
      /* Timers Init */
      // svg
      this.animateEyesTimer.interval = 1000;
      this.animateHeadTimer.interval = 2000;
      // score
      this.scoreIncTimer.interval = 350;
      this.scoreIncTimer.startedAt = Date.now();
      // this.scoreSaverTimer.interval = 30000;
      // this.scoreSaverTimer.startedAt = Date.now();
      localStorage.setItem("timers", JSON.stringify(this));
    }


    if(!app.timers.started){
      if(isCheckIn === false){
        this.animateEyesTimer.obj = setInterval(function(){ app.timers.animateEyesTimer.task() }, this.animateEyesTimer.interval);
        this.animateHeadTimer.obj = setInterval(function(){ app.timers.animateHeadTimer.task() }, this.animateHeadTimer.interval);
        console.log("[TIMER_START]:: SVGs started ::");
      }
      if(isAnimationOnly === false){
        this.scoreIncTimer.obj = setInterval(function(){ app.timers.scoreIncTimer.task() },   this.scoreIncTimer.interval);
        console.log("[TIMER_START]:: INC started ::");
        app.timers.started = true;
      }
      // this.scoreSaverTimer.obj = setInterval(function(){ app.timers.scoreSaverTimer.task() }, this.scoreSaverTimer.interval);
    }else{
      console.log("[TIMER]:: started == true ::");
    }

  };

  app.userData.storeProfile = function (obj){
    obj.score = obj.score == null ? 0 : obj.score;
    // console.log(obj.score);
    localStorage.setItem("profile", JSON.stringify(obj));
  };

  app.userData.getFromLS= function (key){
    return JSON.parse(localStorage.getItem(key));
  };

  // app.userData.setStoredScore = function (score){
  //   var profile = app.userData.getProfile()
  //   profile.score = score;
  //   app.userData.storeProfile(profile);
  // };

  app.createUser = function(){
    console.log("[app]:: New profile");
    var reqObj = {}, isValid={};
    var userFormdata = document.querySelector('form').querySelectorAll("input");
    userFormdata.forEach(function(input){
      if(input.type === "text"){
          if(input.checkValidity() == false){
            console.log(":: Invalid username ::");
          }else{
            isValid.username = true;
            reqObj.name = input.value
          }
      }else{
          if(!input.checked){
              console.log(":: Not checked ::");
          }else{
            isValid.sex = true;
            reqObj.sex = input.value === "male" ? 1 : 0;
          }
      }
    });
    if(!(isValid.username && isValid.sex) ){
      showToast("Invalid username or sex")
      console.log(":: Invalid FORM :: ", isValid);
      return;
    }
    initCharAndMenu(reqObj.sex);
    app.doRequest('POST', {name: reqObj.name, sex: reqObj.sex}, 'user');
  };

  app.getCheckInRoom = function(){
    var checkInRoom = null;
    var checkIn = app.userData.getFromLS("checkIn");
    var sel = document.getElementById("selectBeaconToAdd");
    for (var i = 0; i < sel.options.length; i++) {
      if(sel.options[i].value == checkIn.room){
         checkInRoom = sel.options[i].innerHTML;
      }
    }
    return checkInRoom;
  }

  app.checkIn = function(roomId){
    app.doRequest('POST', {uuid: profile.uuid, room: roomId}, 'checkIn');
  };

  app.setCheckOut = function(room){
    // store checkIn
    localStorage.setItem("checkIn", JSON.stringify( {room: room, at: Date.now()} ));
    // change UI to checkOut
    app.checkInOut(0);
    app.scoreBox.firstElementChild.innerHTML = "Studying in " + app.getCheckInRoom();
  };

  app.checkOut = function(){
    app.doRequest('POST', {uuid: profile.uuid}, 'checkOut');
  };

  // After checking-OUT
  app.setCheckIn = function(){
    // store score earned
    var checkIn = app.userData.getFromLS("checkIn");
    if(checkIn){
      app.scoreBox.firstElementChild.innerHTML = "Score";
      profile.score = Number(app.score.getAttribute('data-score'));
      app.timers.started = false;
      clearInterval(app.timers.scoreIncTimer.obj);
      //var score =  profile.score + Math.floor((Date.now() - checkIn.at) / app.timers.scoreIncTimer.interval);
      // profile.score = score ;
      localStorage.removeItem("checkIn");
      app.userData.storeProfile(profile);
      // change UI to checkOut
      app.checkInOut(1);
    }

  };



  // update rooms list
  app.updateRoomList = function (response){

    var newObj = response.body.reduce( ( result, { uname, rname} ) => {
      if ( ! result[ rname ] ) result[ rname ] = { ulist: [] }; // Create new group
      result[ rname ].ulist.push( uname ); // Append to group
      return result;
    }, {} ) ;

    var rooms = {};
    Object.keys(newObj).forEach(function(key){
      rooms[key] = {
        names: newObj[key].ulist.slice(0, 3),
        count: newObj[key].ulist.length
      }
    });

    // clean empty rooms
    Object.keys(app.visibleRooms).forEach(function(room_key){
      var room_name = room_key.replace("room_", "").split(/(?=.{1}$)/).join(' ');
      if(!rooms.hasOwnProperty(room_name)){
        var roomsList = document.getElementById("roomsList");
        var roomHTML = document.getElementById(room_key);
        roomsList.removeChild(roomHTML);
        delete app.visibleRooms[room_key];
        console.log(room_key, " ROOM Removed");
      }
    });

    // create/update rooms
    Object.keys(rooms).forEach(function(room_name) {
      var room_key = "room_" + room_name.replace(" ", "");
      var roomVR = app.visibleRooms[room_key];
      var roomNames = rooms[room_name].names.join().replace(/,/g, ', ');
      var roomCount = (rooms[room_name].count - 3) < 0 ? '0' : (rooms[room_name].count - 3);
      if (!roomVR) {
        var roomHTML = app.roomItemTemplate.cloneNode(true);
        roomHTML.classList.remove('roomItemTemplate');
        roomHTML.setAttribute("id", room_key);
        roomHTML.removeAttribute('hidden');
        roomHTML.querySelector('.roomName').textContent = room_name;
        roomHTML.querySelector('.roomInfo').innerHTML = "<label>" +  roomNames + "</label> and other "+
                                                        roomCount + " people</span>";
        app.roomContainer.appendChild(roomHTML);
        app.visibleRooms[room_key] = roomHTML;
      }else{
        var roomHTML = document.getElementById(room_key);
        roomHTML.querySelector('.roomName').textContent = room_name;
        roomHTML.querySelector('.roomInfo').innerHTML = "<label>" +  roomNames + "</label> and other "+
                                                        roomCount + " people</span>";
      }
    });
  };


  // select the proper UI updater method
  app.updateFieldSwitcher = function(results, fromCache=false){
    switch (results.field) {
      /* On getRooms response */
      case "getRooms":
        app.updateRoomList(results.response);
        break;

      /* On User creation response */
      case "user":
        profile = results.response.body;
        app.userData.storeProfile(profile);
        app.svg.store("char");
        app.timers.startTimers(false, true);
        app.pages.hide(app.pages.login);
        app.pages.show(app.pages.homepage);
        break;

      /* On User check-In */
      case "checkIn":
        app.timers.startTimers(true, false);
        app.setCheckOut(results.response.body);
        break;

      /* On User check-Out */
      case "checkOut":
        app.setCheckIn();
        break;

      default:
        console.log(":: Dunno What to do!");
    }
  };




  /*****************************************************************************
   *
   * Methods for dealing with the model
   *
   ****************************************************************************/

  /*
   * Gets a forecast for a specific city and updates the card with the data.
   * getForecast() first checks if the weather data is in the cache. If so,
   * then it gets that data and populates the card with the cached data.
   * Then, getForecast() goes to the network for fresh data. If the network
   * request goes through, then the card gets updated a second time with the
   * freshest data.
   */
  app.doRequest = function(reqType, apiParams, field) {
    var url = SERVER + '/api/' + field;
    var params = apiParams === null ? null : JSON.stringify(apiParams);

    // Cache logic
    if ('caches' in window) {
      /*
       * Check if the service worker has already cached this city's weather
       * data. If the service worker has the data, then display the cached
       * data while the app fetches the latest data.
       */
      caches.match(url).then(function(response) {
        if (response) {
          response.json().then(function updateFromCache(jsonObj) {
            var results = {
              field: field,
              response: jsonObj
            };
            app.updateFieldSwitcher(results, true);
          });
        }
      });
    }

    // Fetch the latest data.
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE) {
        if (request.status === 200) {
          var results = {
            field: field,
            response: JSON.parse(request.response)
          };
          app.updateFieldSwitcher(results);
        }
        // console.log("[XHR]:: UPDATE with fresh data");
      } else {
        // Return the initial weather forecast since no data is available.
        // console.log("[XHR]:: NO data is available");
        //app.updateForecastCard(initialWeatherForecast);
      }
    };

    request.open(reqType, url);
    // if(apiParams !== null){
    //   // request.setRequestHeader("Content-type", 'application/json; charset=utf-8');
    // }
    // console.log("[XHR]::params ", params);
    request.send(params);

  };

  // Iterate all of the cards and attempt to get the latest forecast data
  app.updateAllRooms = function() {
    app.doRequest('GET', null, 'getRooms');
  };

  // TODO add saveSelectedCities function here
  app.saveSelectedCities = function() {
    var selectedCities = JSON.stringify(app.selectedCities);
    localStorage.selectedCities = selectedCities;
  };



  // TODO uncomment line below to test app with fake data
  // app.updateForecastCard(initialWeatherForecast);


  // TODO add startup code here
  var profile = app.userData.getFromLS("profile");
  var checkIn = app.userData.getFromLS("checkIn");
  if(profile){
    console.log("[app]:: Profile found ", profile);

    //load customized character
    app.svg.loadintoElem(app.svg.domElem("char"));
    app.svg.loadintoElem(app.svg.domElem("charStore"));
    app.menu.restore();

    //initCharAndMenu
    initCharAndMenu(profile.sex, true);

    // TO MOVE, on checkIn..
    if(checkIn){
      app.scoreBox.firstElementChild.innerHTML = "Studying in " + app.getCheckInRoom();
      app.timers.startTimers(false, false);
      app.checkInOut(0);
    }else{
      //set UI score
      app.score.textContent = nFormatter(Number(profile.score), 2);
      app.timers.startTimers(false, true);
    }

    //show home page
    app.pages.hide(app.pages.login);
    app.pages.show(app.pages.homepage);

  }


  // TODO add service worker code here
  // TO DELETE
  var host = window.location.host;
  var devhost = host === "localhost" ? '' : "https://tsiu.ga/pwa"

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register(devhost + '/service-worker.js')
             .then(function() {
               console.log('Service Worker Registered');
               setTimeout("initNotifications()", 2500);
             });
  }
})();


$('#menu li a').click(function(e) {
    e.preventDefault();
    $('a').removeClass('active');
    $(this).addClass('active');
});

