/**
 * Created by ac on 6-6-17.
 */

 const SERVER = "http://172.16.22.129:8000";

/**
 * Initialise application data
 */
function init() {

    // Check if this is the first run

    var uuid = localStorage.getItem("uuid");

    if(uuid == null){

        // uuid negotiation

        $.ajax({
                url: SERVER + "/api/user",
                type: "PUT",
                data: "name=John&sex=1",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                success: function(response){
                    // store the uuid in local storage
                    if(response.error === false){
                        uuid = response.body;
                        localStorage.setItem("uuid", uuid);
                        localStorage.setItem("score", 0);
                        console.log(response);
                    }else{
                        //todo
                    }
                }
            }
        );
    } else {
        console.log("[uuid]: " + uuid);
    }
}


/**
 * check-in room
 */
function checkIn(room) {

    var uuid = localStorage.getItem("uuid");

    $.ajax({
            url: SERVER + "/api/checkIn",
            type: "PUT",
            data: "uuid=" + uuid + "&room="+room,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            success: function(response){
                if(response.error === false){
                    console.log(response);
                }else{
                    //todo
                }
            }
        }
    );
}


/**
 * check-out room
 */
function checkOut() {

    var uuid = localStorage.getItem("uuid");

    $.ajax({
            url: SERVER + "/api/checkOut",
            type: "PUT",
            data: "uuid=" + uuid,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            success: function(response){
                if(response.error === false){
                    console.log(response);
                }else{
                    //todo
                }
            }
        }
    );
}


/**
 * get rooms info
 */
function getRooms() {
    $.ajax({
            url: SERVER + "/api/getRooms",
            type: "GET",
            success: function(response){
                if(response.error === false){
                    console.log(response);
                }else{
                    //todo
                }
            }
        }
    );
}
