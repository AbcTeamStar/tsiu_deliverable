

function initNotifications(){
    if (!("Notification" in window)) {
        alert('Notification API not supported.');
        return;
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        displayNotification();
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                displayNotification();
            }
        });
    }
 /* if(openedInApp){
    Notification.requestPermission(function(status) {
        console.log('Notification permission status:', status);
        displayNotification();
    });
  }*/
}

function displayNotification() {
  if (Notification.permission == 'granted') {
    navigator.serviceWorker.getRegistration().then(function(reg) {
      var options = {
        badge: '/images/chrome-touch-icon-192x192.png',
        body: 'remember to check-out when you leave!',
        icon: 'images/chrome-touch-icon-192x192.png',
        vibrate: [100, 50, 100],
        data: {
          dateOfArrival: Date.now(),
          primaryKey: 1
        },
        actions: [
          {action: 'explore', title: 'Open',
            icon: 'https://bit.ly/2scvE2A'},
          {action: 'close', title: 'Close',
            icon: 'https://bit.ly/2scvE2A'},
        ]
      };
      reg.showNotification('Reminder!', options);
    });
  }
}
