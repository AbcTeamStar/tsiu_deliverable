/**
 * Created by buzzo on 6/14/17.
 */
var capelli = ["#capelli1", "#capelli2", "#capelli3", "#capelli4", "#capelli5"];
var bocca = ["#bocca1", "#bocca2", "#bocca3"];
var noses = ["#naso1", "#naso2", "#naso3"];
var baffi = ["#baffi1", "#baffi2", "#baffi3", "#baffi4"];
var orecchini = ["#orecchini1", "#orecchini2", "#orecchini3"];
var fiocchi = ["#fiocco1", "#fiocco2"];
var occhiali = ["#occhiali1", "#occhiali2", "#occhiali3"];
var items = ["#pc", "#book", "#writing"];

var charStore = "#charStore";

var nose1 = $('#nose1');
var nose2 = $('#nose2');
var nose3 = $('#nose3');

var eyesColor1 = $('#eyesColor1');
var eyesColor2 = $('#eyesColor2');
var eyesColor3 = $('#eyesColor3');

var hairColor1 = $('#hairColor1');
var hairColor2 = $('#hairColor2');
var hairColor3 = $('#hairColor3');
var hairColor4 = $('#hairColor4');
var hair1 = $('#hair1');
var hair2 = $('#hair2');
var hair3 = $('#hair3');
var hair4 = $('#hair4');
var hair5 = $('#hair5');

var mustColor1 = $('#mustColor1');
var mustColor2 = $('#mustColor2');
var mustColor3 = $('#mustColor3');
var mustColor4 = $('#mustColor4');
var noMust = $('#noMust');
var mustache1 = $('#mustache1');
var mustache2 = $('#mustache2');
var mustache3 = $('#mustache3');
var mustache4 = $('#mustache4');


var noFiocco = $('#noFiocco');
var fio1 = $('#fio1');
var fio2 = $('#fio2');

var noOr = $('#noOr');
var or1 = $('#or1');
var or2 = $('#or2');
var or3 = $('#or3');

var noGlasses = $('#noGlasses');
var glasses1 = $('#glasses1');
var glasses2 = $('#glasses2');
var glasses3 = $('#glasses3');

var torsoColor1 = $('#torsoColor1');
var torsoColor2 = $('#torsoColor2');
var torsoColor3 = $('#torsoColor3');

var pcItem = $('#pcItem');
var bookItem = $('#bookItem');
var penItem = $('#penItem');

var whiteSkin = $('#whiteSkin');
var blackSkin = $('#blackSkin');
var yellowSkin = $('#yellowSkin');


function checkPurchase(item, funz, p1, p2, p3) {
    if (item.hasClass("unpurchased")) {
        return;
    } else {
        funz(p1, p2, p3);
    }
}


nose1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, noses, 0);
});
nose2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, noses, 1);
});
nose3.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, noses, 2);
});


eyesColor1.click(function () {
    changeColor(charStore, "#pupillaL", "#00858F");
    changeColor(charStore, "#pupillaR", "#00858F");
});
eyesColor2.click(function () {
    checkPurchase($(this), changeColor, charStore, "#pupillaL", "#32A30D");
    checkPurchase($(this), changeColor, charStore, "#pupillaR", "#32A30D");
});
eyesColor3.click(function () {
    checkPurchase($(this), changeColor, charStore, "#pupillaL", "#222222");
    checkPurchase($(this), changeColor, charStore, "#pupillaR", "#222222");
});

hairColor1.click(function () {
    capelli.forEach(function (element) {
        checkPurchase(hairColor1, changeColor, charStore, element, "#002334");
    })
});
hairColor2.click(function () {
    capelli.forEach(function (element) {
        checkPurchase(hairColor2, changeColor, charStore, element, "#FFD883");
    })
});
hairColor3.click(function () {
    capelli.forEach(function (element) {
        checkPurchase(hairColor3, changeColor, charStore, element, "#753E23");
    })
});
hairColor4.click(function () {
    capelli.forEach(function (element) {
        checkPurchase(hairColor4, changeColor, charStore, element, "#F57E20");
    })
});
hair1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, capelli, 0);
});
hair2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, capelli, 1);
});
hair3.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, capelli, 2);
});
hair4.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, capelli, 3);
});
hair5.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, capelli, 4);
});

mustColor1.click(function () {
    baffi.forEach(function (element) {
        checkPurchase(mustColor1, changeColor, charStore, element, "#002334");
    })
});
mustColor2.click(function () {
    baffi.forEach(function (element) {
        checkPurchase(mustColor2, changeColor, charStore, element, "#FFD883");
    })
});
mustColor3.click(function () {
    baffi.forEach(function (element) {
        checkPurchase(mustColor3, changeColor, charStore, element, "#753E23");
    })
});
mustColor4.click(function () {
    baffi.forEach(function (element) {
        checkPurchase(mustColor4, changeColor, charStore, element, "#F57E20");
    })
});

noMust.on("click", function () {
    baffi.forEach(function (element) {
        hideElement(charStore, element);
    })
});
mustache1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, baffi, 0);
});
mustache2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, baffi, 1);
});
mustache3.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, baffi, 2);
});
mustache4.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, baffi, 3);
});

noFiocco.on("click", function () {
    fiocchi.forEach(function (element) {
        hideElement(charStore, element);
    })
});
fio1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, fiocchi, 0);
});
fio2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, fiocchi, 1);
});

noOr.on("click", function () {
    orecchini.forEach(function (element) {
        hideElement(charStore, element);
    })
});
or1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, orecchini, 0);
});
or2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, orecchini, 1);
});
or3.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, orecchini, 2);
});


noGlasses.on("click", function () {
    occhiali.forEach(function (element) {
        hideElement(charStore, element);
    })
});
glasses1.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, occhiali, 0);
});
glasses2.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, occhiali, 1);
});
glasses3.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, occhiali, 2);
});

torsoColor1.click(function () {
    checkPurchase($(this), changeColor, charStore, "#body", "#0094BB");
});
torsoColor2.click(function () {
    checkPurchase($(this), changeColor, charStore, "#body", "#3C9941");
});
torsoColor3.click(function () {
    checkPurchase($(this), changeColor, charStore, "#body", "#991C96");
});


pcItem.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, items, 0);
});
bookItem.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, items, 1);
});


penItem.on("click", function () {
    checkPurchase($(this), selectByGroup, charStore, items, 2);
});
blackSkin.click(function () {
    skinColor(charStore, "#824b46", "#512724");
    changeColor(charStore, "#naso1", "#512724");
    changeColor(charStore, "#bocca1", "#512724");
});

whiteSkin.click(function () {
    skinColor(charStore, "#F6C09C", "#EE927E");
});

yellowSkin.click(function () {
    skinColor(charStore, "#f6d79c", "#eeb37e");
});


(function () {
    'use strict';


    var dialogP = {
        addDialog: document.querySelector('.dialog-container-purchase')
    };


    $('.unpurchased').on('click', function () {
        console.log("::unpurchased pressed");
        if (!$(this).hasClass("unpurchased")) {
            console.log(":: OLD listener.. return!");
            return;
        }
        var price = this.getAttribute('data-price')
        var buyItem = document.getElementById('buyItem');
        var showPrice = document.getElementById("showPrice");
        buyItem.setAttribute("data-current-price", price);
        buyItem.setAttribute("data-caller-id", this.getAttribute("id"));
        showPrice.innerHTML = "Cost: " + price;
        dialogP.toggleAddDialog(true);
    });

    $('#buyItem').on('click', function () {
        dialogP.toggleAddDialog(false);
    });

    $('#noBuy').on('click', function () {
        dialogP.toggleAddDialog(false);
    });


    dialogP.toggleAddDialog = function (visible) {
        if (visible) {
            dialogP.addDialog.classList.add('dialog-container-purchase--visible');
        } else {
            dialogP.addDialog.classList.remove('dialog-container-purchase--visible');
        }
    };

})();
