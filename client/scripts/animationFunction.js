var pivots = [
    [211, 420],
    [211, 380]
];

var posEyes = [280, 210];

var posPen = [
    [193, 555],
    [190, 555]
];

/** Change color of an element. */
function changeColor(mainId, elementId, color) {
    Snap.select(mainId + " " + elementId).attr({fill: color});
}

/** Hide an element. */
function hideElement(mainId, elementId) {
    Snap.select(mainId + " " + elementId).attr({opacity: 0});
}

/** Show an element */
function showElement(mainId, elementId) {
    Snap.select(mainId + " " + elementId).attr({opacity: 1});
}

/** Select only one element for group. */
function selectByGroup(mainId, groupList, number) {
    groupList.forEach(function (element) {
        hideElement(mainId, element);
    });
    showElement(mainId, groupList[number]);
}


function blinkEyes(mainId, n) {
    var eyes = Snap.select(mainId + " #occhi");
    var myMatrix = new Snap.Matrix();
    var myMatrix2 = new Snap.Matrix();
    myMatrix.translate(0, posEyes[n]);
    myMatrix.scale(1, 0.1);
    myMatrix2.scale(1);
    eyes.animate({transform: myMatrix}, 50, mina.linear, function () {
        eyes.animate({transform: myMatrix2}, 50, mina.linear)
    });
};

function moveEyes(mainId, x, y, time) {
    var eyeL = Snap.select(mainId + " #pupillaL");
    var eyeR = Snap.select(mainId + " #pupillaR");
    var m = new Snap.Matrix();
    m.translate(x, y);
    eyeL.animate({transform: m}, time, mina.linear);
    eyeR.animate({transform: m}, time, mina.linear);

};


function moveHead(mainId, n) {
    var head = Snap.select(mainId + " #head");

    head.animate({transform: "r" + [2, pivots[n]]}, 1000, mina.backin, function () {
        head.animate({
            transform: "r" + [-2, pivots[n]]
        }, 1000, mina.backin);
    });
}

function movePen(mainId, n) {
    var pen = Snap.select(mainId + " #pen");

    pen.animate({transform: "r" + [5, posPen[n]]}, 200, mina.linear, function () {
        pen.animate({
            transform: "r" + [-5, posPen[n]]
        }, 200, mina.linear);
    });
}


function skinColor(mainId, color1, color2) {
    changeColor(mainId, "#viso", color1);
    changeColor(mainId, "#mano1", color1);
    changeColor(mainId, "#mano2", color1);
    changeColor(mainId, "#esternoOrecchie", color1);
    changeColor(mainId, "#internoOrecchie", color2);
    changeColor(mainId, "#collo", color2);
    changeColor(mainId, "#naso2", color2);
    changeColor(mainId, "#naso1", "#EE3965");
    changeColor(mainId, "#bocca1", "#C52C54");

}
