/**
 * Created by buzzo on 6/13/17.
 */

var mainList = $('#mainList');
var appearanceList = $('#appearanceList');
var appearanceItem = $('#appearance');
var goToMain =$('.goToMain');
var face =$('#face');
var faceList =$('#faceList');
var goToAppearance = $('.goToAppearance');
var eyesList = $('#eyesList');
var eyes = $('#eyes');
var hairList = $('#hairList');
var hair = $('#hair');
var nose = $("#nose");
var noseList = $('#noseList');
var mustache = $('#mustache');
var mustacheList = $('#mustacheList');
var glasses = $('#glasses');
var glassesList = $('#glassesList');

var homework = $('#homework');
var homeworkList = $('#homeworkList');
var torso = $('#torso');
var torsoList = $('#torsoList');

var fiocco = $('#fiocco');
var fioccoList = $('#fioccoList');

var or = $('#or');
var orList = $('#orList');


function hideList(list){
    list.addClass("hidden");
}

function showList(list){
    list.removeClass("hidden");
}


appearanceItem.on("click", function(){
    hideList(mainList);
    showList(appearanceList);
});

goToMain.on("click", function(){
    hideList(appearanceList);
    hideList(glassesList);
    hideList(homeworkList);
    hideList(torsoList);
    hideList(orList);
    hideList(fioccoList);
    showList(mainList);

});

face.on("click", function(){
    hideList(appearanceList);
    showList(faceList);
});

goToAppearance.on("click", function(){
    hideList(faceList);
    hideList(eyesList);
    hideList(hairList);
    hideList(noseList);
    hideList(mustacheList);
    showList(appearanceList);
});

eyes.on("click", function(){
    hideList(appearanceList);
    showList(eyesList);
});

hair.on("click", function(){
    hideList(appearanceList);
    showList(hairList);
});

nose.on("click", function(){
    hideList(appearanceList);
    showList(noseList);
});

mustache.on("click", function(){
    hideList(appearanceList);
    showList(mustacheList);
});

glasses.on("click", function(){
    hideList(mainList);
    showList(glassesList);
});

homework.on("click", function(){
    hideList(mainList);
    showList(homeworkList);
});

torso.on("click", function(){
    hideList(mainList);
    showList(torsoList);
});


fiocco.on("click", function(){
    hideList(mainList);
    showList(fioccoList);
});

or.on("click", function(){
    hideList(mainList);
    showList(orList);
});
