<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->post('api/user', [
  'middleware' => 'cors', 'uses' => 'UserController@create'
]);

$app->post('api/checkIn', [
    'middleware' => 'cors', 'uses' => 'UserController@checkIn'
]);

$app->post('api/checkOut', [
    'as' => 'checkOut', 'uses' => 'UserController@checkOut'
]);

$app->get('api/getRooms', [
    'middleware' => 'cors', 'uses' => 'RoomController@getRooms'
]);

$app->get('/', function () use ($app) {
	return redirect('pwa/');
})/*->middleware('cors')*/;


